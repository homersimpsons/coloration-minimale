Coloration Minimale
===================

Inroduction
-----------

This is a school project, the main goal was to generate polynomial from straightlines then apply minimal coloration on it.
Due to time constraints the goal was the same but without minimal coloration.

Specifications
--------------

- Find polynomial from straightlines described
- Visual show of steps to obtain the resuls (i.e. the algorithm)

Development
-----------

- Download the project: `git clone`
- Run and observe: `./gradlew run`

Contributors
------------

Remi Fleurance and Guillaume Alabré
