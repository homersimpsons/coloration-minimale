/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colorationminimale;

/**
 * @author guillaume.alabre et remi.fleurance
 */
public class Segment {
    private final int A,B;
    boolean selected;
    
    // A et B doivent être dans l'ordre croissant !
    Segment(int _A, int _B) {
        A = _A;
        B = _B;
        selected = false;
    }
    
    public boolean isInSegement(int C) {
        return (C == A || C == B);
    }
    
    public int otherExtremity(int C) {
        if (C==A) {
            return B;
        }
        return A;
    }

    public int getA() {
        return A;
    }

    public int getB() {
        return B;
    }
    
    public boolean isSetected() { return selected; }
    public void setSelected(boolean _selected) { selected = _selected; }
}
