/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colorationminimale;

import java.awt.Color;
import java.awt.Graphics;
import static java.lang.Math.round;
import javax.swing.JPanel;

/**
 * @author guillaume.alabre et remi.fleurance
 */
class GeoFrame extends JPanel {
    Point center;
    double scale;
    Graphics pencil;
    Scene scene;

    public GeoFrame(){
        center= new Point();
        scale= 10;
    }
    
    public void setCenter(Point c){
        center.x= c.x;
        center.y= c.y;
    }

    public void setScale(double s){
        scale= s;
    }
    
    public void setScene(Scene _scene) {
        scene = _scene;
        setCenter(scene.getCenter());
    }
    
    
    // Scene to screen transformation:
    public int abs(double x){ return this.getWidth()/2 + (int)round((x-center.x)*scale); }
    public int ord(double y){ return this.getHeight()/2 - (int)round((y-center.y)*scale); }
    public double revAbs(int x){ return (double) ((double) x - (double) this.getWidth()/2)/scale + (double) center.x; }
    public double revOrd(int y){ return (double) ( - ((double) y - (double) this.getHeight()/2)/scale) + (double) center.y; }
    
    // painting method:
    @Override
    public void paint( Graphics g ){
        pencil= g;
        pencil.setColor(Color.white);
        pencil.fillRect(0, 0, this.getWidth(), this.getHeight());
        drawScene();
    }
    
    public void drawScene() {
        Color[] possibleColor = {Color.CYAN, Color.DARK_GRAY, Color.GREEN, Color.LIGHT_GRAY, Color.MAGENTA, Color.PINK, Color.YELLOW, Color.ORANGE};
        
        if (scene.getPolygons() != null) {
            Polygon[] Polygons = scene.getPolygons();
            for (int i = 0; i < Polygons.length; i++) {
                if (Polygons[i] != null) {
                    pencil.setColor(Polygons[i].isSetected() ? Color.BLUE : possibleColor[i%possibleColor.length]);
                    fillPolygon(Polygons[i]);
                }
            }
        }
        
        if (scene.getSLs() != null) {
            StraightLine[] SLs = scene.getSLs();
            for (int i = 0; i < SLs.length; i++) {
                if (SLs[i] != null) {
                    pencil.setColor(SLs[i].isSetected() ? Color.RED : Color.BLACK);
                    drawStraightLine(SLs[i]);
                }
            }
        }
        
        Point[] Inters = scene.getIntersections();
        if (scene.getSegments() != null) {
            Segment[] Segments = scene.getSegments();
            for (int i = 0; i < Segments.length; i++) {
                if (Segments[i] != null) {
                    pencil.setColor(Segments[i].isSetected() ? Color.RED : Color.GRAY);
                    drawLine(Inters[Segments[i].getA()], Inters[Segments[i].getB()]);
                }
            }
        }
        
        if (scene.getIntersections() != null) {
            pencil.setColor(Color.blue);
            for (int i = 0; i < Inters.length; i++) {
                if (Inters[i] != null) {
                    if (Inters[i].isSetected()) {
                        drawCircle(Inters[i], 4);
                    } else {
                        drawPoint(Inters[i]);
                    }
                }
            }
        }
        
    }
    
    public void drawCircle(Point O, int rayon) {
        pencil.fillOval(abs(O.x) - rayon, ord(O.y) - rayon, 2*rayon, 2*rayon);
    }
    
    public void drawPoint( Point p )
    {
        pencil.drawLine( abs(p.x)-2, ord(p.y)-2, abs(p.x)+2, ord(p.y)+2 );
        pencil.drawLine( abs(p.x)+2, ord(p.y)-2, abs(p.x)-2, ord(p.y)+2 );
    }

    public void drawLine( Point A, Point B )
    {
        pencil.drawLine(abs(A.x), ord(A.y), abs(B.x), ord(B.y));
    }
    
    public void drawStraightLine(StraightLine L) // Dessine une droite à travers toute la fenètre
    {
        Point A,B;
        int ordinate = ord(L.getSlope()*revAbs(this.getWidth())+L.getIntercept());
        if (ordinate < this.getHeight() && ordinate > 0) {
            A = new Point(this.getWidth(), ordinate);
        } else if (L.getSlope() > 0) {
            
            A = new Point(abs((revOrd(0) - L.getIntercept())/L.getSlope()), 0);
        } else {
            A = new Point(abs((revOrd(this.getHeight()) - L.getIntercept())/L.getSlope()), this.getHeight());
        }
        
        ordinate = ord(L.getSlope()*revAbs(0)+L.getIntercept());
        if (ordinate < this.getHeight() && ordinate > 0) {
            B = new Point(0, ordinate);
        } else if (L.getSlope() > 0) {
            B = new Point(abs((revOrd(this.getHeight()) - L.getIntercept())/L.getSlope()), this.getHeight());
        } else {
            B = new Point(abs((revOrd(0) - L.getIntercept())/L.getSlope()), 0);
        }
        pencil.drawLine((int) A.x, (int) A.y, (int) B.x, (int) B.y);
    }
    
    public void fillPolygon( Polygon P ) {
        int[] x = new int[P.pointInd.length];
        int[] y = new int[P.pointInd.length];
        for( int i = 0; i < P.pointInd.length; i++ ){
            x[i] = abs(scene.getP(P.pointInd(i)).x);
            y[i] = ord(scene.getP(P.pointInd(i)).y);
        }
        pencil.fillPolygon(x, y, P.pointInd.length);
    }
}