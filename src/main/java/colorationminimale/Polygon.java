/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colorationminimale;

/**
 * @author guillaume.alabre et remi.fleurance
 */
public class Polygon {
    int[] pointInd;
    boolean selected;
    
    public Polygon(){
        pointInd= new int[1];
        pointInd[0]= 0;
        selected = false;
    }

    public Polygon(int n){
        pointInd= new int[n];
        for( int p : pointInd )
            p= 0;
        selected = false;
    }
    
    public Polygon(int[] tab){
        pointInd= new int[tab.length];
        for(int i= 0; i < pointInd.length ; ++i) {
            pointInd[i]= tab[i];
        }
    }
    
    public int pointInd(int i){
        return pointInd[i];
    }
    
    public int length() {
        return pointInd.length;
    }
    
    public boolean isSetected() { return selected; }
    public void setSelected(boolean _selected) { selected = _selected; }
}
