/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colorationminimale;

/**
 * @author guillaume.alabre et remi.fleurance
 */
public class Point {
    double x, y;
    boolean selected;
    
    public Point(){
        this.x= 0;
        this.y= 0;
        selected = false;
    }
    
    public Point(double xx, double yy){
        this.x= xx;
        this.y= yy;
        selected = false;
    }
    
    public Point(Point A){
        this.x= A.x;
        this.y= A.y;
        selected = false;
    }
    
    public Point(Point A, Point B){
        this.x= B.x - A.x;
        this.y= B.y - A.y;
        selected = false;
    }
    
    public double x(){ return x; }
    public double y(){ return y; }

    public void x(int xx){ x= xx; }
    public void y(int yy){ y= yy; }
    
    // Standard Object method:
    @Override
    public String toString(){ return "("+ x + ", "+ y  + ")"; }
    
    public double dist(){ return Math.sqrt( x*x + y*y ); }
    
    public static boolean isTrigo(Point B, Point O, Point A){
        return ((A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x-O.x) + 0.000000001 >= 0); // On minimise les erreurs d'arrondis
    }
    
    public static double dist(Point A, Point B)
    {
        Point AB= new Point(A, B);
        return AB.dist();
    }
    
    // Pour l'animation
    public boolean isSetected() { return selected; }
    public void setSelected(boolean _selected) { selected = _selected; }
}
