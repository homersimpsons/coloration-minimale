/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colorationminimale;

/**
 * @author guillaume.alabre et remi.fleurance
 */
public class StraightLine {
    private final Point A, B;
    private double slope, intercept;
    private boolean selected;
    
    StraightLine(Point _A) {
        A = _A;
        B = new Point();
        generateExtra();
        selected = false;
    }
    
    StraightLine(Point _A, Point _B) {
        A = _A;
        B = _B;
        generateExtra();
        selected = false;
    }
    
    private void generateExtra() {
        slope = (A.y-B.y)/(A.x-B.x);
        intercept = A.y - getSlope() * A.x;
    }
    
    public double getSlope() {
        return slope;
    }
    
    public double getIntercept() {
        return intercept; 
    }
    
    public Point intersect(StraightLine L) {
        if(slope == L.getSlope()) {
            return null;
        }
        double x = (L.getIntercept()-intercept)/(slope - L.getSlope());
        return new Point(x, slope*x + intercept);
    }
    
    public boolean isOnSL(Point C) {
        return Math.abs(C.y - (slope*C.x + intercept)) < 0.00000000000001; // On évite less erreurs d'arrondie
    }
    
    @Override
    public String toString() {
        return "y=" + getSlope() + "*x+" + getIntercept();
    }
    
    public boolean isSetected() { return selected; }
    public void setSelected(boolean _selected) { selected = _selected; }
}
