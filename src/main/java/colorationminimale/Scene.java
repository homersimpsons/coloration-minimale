/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colorationminimale;

/**
 * @author guillaume.alabre et remi.fleurance
 */
public class Scene {
    private final StraightLine[] SLs;
    private Polygon[] Polygons;
    private Point[] Intersections;
    private Point center;
    private Segment[] Segments;
    private final GeoFrame frame;
    
    Scene(StraightLine[] _SLs, GeoFrame _frame)  {
        frame = _frame;
        SLs = _SLs;
        generateIntersections();
//        System.out.println("Intersections : " + Intersections.length);
        generateCenter();
    }
    
    public void run() {
        generateSegments();
//        System.out.println("Segments : " + Segments.length);
        generatePolygons();
//        System.out.println("Polygons : " + Polygons.length);
    }
    
    private void repaint() { // On temporise les repaint
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) { }
        frame.repaint();
    }
    
    /**
     * @param SL Une Droite de la scène
     * @return les points d'intersections triés sur cette droite
     */
    private int[] getPointsOnSL(StraightLine SL) {
        int nbPoint = 0;
        int[] tmpPoint = new int[SLs.length-1];
        for (int i = 0; i < Intersections.length; i++) {
            if (SL.isOnSL(Intersections[i])) {
                tmpPoint[nbPoint] = i;
                nbPoint++;
            }
        }
        int[] Points = new int[nbPoint];
        for(int i = 0; i < Points.length; i++) {
            Points[i] = tmpPoint[i];
        }
        return triPoint(Points);
    }
    
    /**
     * @param liste de points à ordonner
     * @return les points passés en paramètres triés (ordre croissan d'abscisse ou d'ordonnées si égales)
     */
    private int[] triPoint(int[] Points) { // tri bulle
        for (int i = Points.length - 1; i > 0; i-- ) {
            boolean trie = true;
            for (int j = 0; j < i; j++) {
                if(isGreaterPoint(Intersections[Points[j]], Intersections[Points[j+1]])) {
                    int a = Points[j];
                    Points[j] = Points[j+1];
                    Points[j+1] = a;
                    trie = false;
                }
            }
            if(trie) { 
                return Points;
            }
        }
        return Points;
    }
    
    /**
     * @param A point 1
     * @param B point 2
     * @return A > B ?
     */
    private boolean isGreaterPoint(Point A, Point B) {
        if (A.x == B.x) {
            return (A.y > B.y);
        }
        return (A.x > B.x );
    }
    
    // génère les segments de la scène
    private void generateSegments(){
        int nbSegments = 0;
        Segments = new Segment[(SLs.length-1)*(SLs.length-1)-1];
        for (int i = 0; i < SLs.length; i++) {
            int[] indicePoints = getPointsOnSL(SLs[i]);
            for (int j = 0; j < indicePoints.length - 1; j ++) {
                Segments[nbSegments] = new Segment(indicePoints[j], indicePoints[j+1]);
                // On anime
                Segments[nbSegments].setSelected(true);
                repaint();
                Segments[nbSegments].setSelected(false);
                nbSegments++;
            }
        }
        Segment[] tmpSegments = new Segment[nbSegments];
        for(int i = 0; i < tmpSegments.length; i++) {
            tmpSegments[i] = Segments[i];
        }
        Segments = tmpSegments;
    }
    
    // génère les polygons de la scène
    private void generatePolygons() {
        Polygons = new Polygon[Segments.length];
        int nbPolygon =0;
        for(int i = 0; i < Segments.length; i++) {
            Polygon tmpPolygon = generatePolygon(i, Polygons, nbPolygon);
            if (tmpPolygon != null) {
//                System.out.println("ADDED");
                Polygons[nbPolygon] = tmpPolygon;
                Polygons[nbPolygon].setSelected(true);
                repaint();
                Polygons[nbPolygon].setSelected(false);
                nbPolygon++;
            }
        }
        Polygon[] tmpPolygons = new Polygon[nbPolygon];
        for(int i = 0; i < nbPolygon; i++) {
            tmpPolygons[i] = Polygons[i];
        }
        Polygons = tmpPolygons;
        
    }
    
    /**
     * @param i segment concerné
     * @param tmpPolygons la liste actuels des polygons
     * @param nbPolygon le nombre de polygons déjà générés
     * @return le polygon basé sur le segment i (ou null si invalide)
     */
    public Polygon generatePolygon(int i, Polygon[] tmpPolygons, int nbPolygon) {
        int[] indexConv = new int[Segments.length + 1];
        indexConv[0] = Segments[i].getA();
        int current = Segments[i].getB();
        int nbPoint = 1;
        do {
            indexConv[nbPoint] = current;
            Intersections[indexConv[nbPoint]].setSelected(true); // Animation des triplets de points sélectionnés
            nbPoint++;
            int[] PointsToTest = pointsOnSegmentOnPoint(current);
            current = PointsToTest[0];
            int start = 1;
            if (current == indexConv[nbPoint-2]) {
                start = 2;
                current = PointsToTest[1];
            }
            Intersections[current].setSelected(true); // Animation des triplets de points sélectionnés
//            System.out.println("current : " + current + " : " + indexConv[nbPoint-1]);
            for (int j = start; j < PointsToTest.length; j++) {
                int p = PointsToTest[j];
                
                // Animation des triplets de points sélectionnés
                Intersections[p].setSelected(true);
                repaint();
                Intersections[p].setSelected(false);
                
//                System.out.println(indexConv[nbPoint-1] + " " + current + " " + p + " : " + Point.isTrigo(Intersections[indexConv[nbPoint-1]], Intersections[current], Intersections[p]));
                if(current != p && indexConv[nbPoint-2] != p  && Point.isTrigo(Intersections[indexConv[nbPoint-2]], Intersections[indexConv[nbPoint-1]], Intersections[p]) && Point.isTrigo(Intersections[indexConv[nbPoint-1]], Intersections[current], Intersections[p])){ // && Point.isTrigo(Intersections[indexConv[nbPoint-2]], Intersections[indexConv[nbPoint-1]], Intersections[p])
                    Intersections[current].setSelected(false); // Animation des triplets de points sélectionnés
                    current = p;
                    Intersections[current].setSelected(true); // Animation des triplets de points sélectionnés
                }
            }
            Intersections[current].setSelected(false); // Animation des triplets de points sélectionnés
            Intersections[indexConv[nbPoint-1]].setSelected(false); // Animation des triplets de points sélectionnés
            if (!Point.isTrigo(Intersections[indexConv[nbPoint-2]], Intersections[indexConv[nbPoint-1]], Intersections[current])) {
//                System.out.println("INVALID");
                return null;
            }
            if(nbPoint == 2) {
                if(isAlreadyPolygon(indexConv[0], indexConv[1], current, tmpPolygons, nbPolygon)) {
//                    System.out.println("ALREADY");
                    return null;
                }
            }
        } while (current != indexConv[0]);
        if(nbPoint == 2) {
//            System.out.println("LOOP");
            return null;
        }
        Polygon convexe = new Polygon(nbPoint);
        for (int j = 0; j < convexe.pointInd.length; j++) {
            convexe.pointInd[j] = indexConv[j];
        }
        return convexe;
    }
    
    /**
     * @param indexPoint point concerné
     * @return les points adjacents (via segments) à ce point
     */
    private int[] pointsOnSegmentOnPoint(int indexPoint) {
        int[] tmpPoint = new int[Intersections.length - 1];
        int nbPoint = 0;
        for(int i = 0; i < Segments.length; i++) {
            if(Segments[i].isInSegement(indexPoint)) {
                // On anime la vue
                Segments[i].setSelected(true);
                repaint();
                Segments[i].setSelected(false);
//                System.out.println("Autre extremite de " + Segments[i].otherExtremity(Segments[i].otherExtremity(indexPoint)) + "    :    " + Segments[i].otherExtremity(indexPoint));
                tmpPoint[nbPoint] = Segments[i].otherExtremity(indexPoint);
                nbPoint++;
            }
        }
        int[] points = new int[nbPoint];
        for(int i = 0; i < nbPoint; i++) {
            points[i] = tmpPoint[i];
        }
        return points;
    }
    
    /**
     * @param p1 1er indice d'intersection
     * @param p2 2eme indice d'intersection
     * @param p3 3eme indice d'intersection
     * @param nbPolygon nbPolygon déjà générés
     * @return le polygone existe-t-il déjà ?
     */
    private boolean isAlreadyPolygon(int p1, int p2, int p3, Polygon[] tmpPolygons, int nbPolygon) {
        for (int i = 0; i < nbPolygon; i++ ) {
//            System.out.println(i + "onzerg");
            for (int j = 0; j < tmpPolygons[i].length(); j++) {
                if(tmpPolygons[i].pointInd(j) == p1 &&
                   tmpPolygons[i].pointInd((j+1)%tmpPolygons[i].length()) == p2 &&
                   tmpPolygons[i].pointInd((j+2)%tmpPolygons[i].length()) == p3) {
                    return true;
                }
            }
        }
        return false;
    }
    
    // génère les intersections de toutes les droites
    private void generateIntersections() {
        int nbInters = 0;
        Point[] tmpInter = new Point[(SLs.length-1)*SLs.length/2];
        for(int i = 0; i < SLs.length - 1; i++) {
            for(int j = i + 1; j < SLs.length; j++) {
                Point inter = SLs[i].intersect(SLs[j]);
                if(inter != null) {
                    if (!mergedIntersection(inter, tmpInter, nbInters)) {
                        tmpInter[nbInters] = inter;
                        nbInters++;
                    }
                }
            }
        }
        Intersections = new Point[nbInters];
        for(int i = 0; i < Intersections.length; i++) {
            Intersections[i] = tmpInter[i];
        }
    }
    
    /**
     * @param inter intersection à tester
     * @param tmpInter intersections générées
     * @param nbInters nombre d'intersection générées
     * @return les intersections doivent-elles être fusionnées ?
     */
    private boolean mergedIntersection(Point inter, Point[] tmpInter, int nbInters) { // évite les problème d'arrondie
        for (int i = 0; i < nbInters; i++) {
            if (Point.dist(inter, tmpInter[i]) < 0.00000000000001) { return true; }
        }
        return false;
    }
    
    // génère le barycentre des intersections
    private void generateCenter() {
        double max_x = Intersections[0].x;
        double max_y = Intersections[0].y;
        double min_x = Intersections[0].x;
        double min_y = Intersections[0].y;
        for (int i = 0; i < Intersections.length; i++) {
            if (Intersections[i].x > max_x) {
                max_x = Intersections[i].x;
            }
            if (Intersections[i].x < min_x) {
                min_x = Intersections[i].x;
            }
            if (Intersections[i].y > max_y) {
                max_y = Intersections[i].y;
            }
            if (Intersections[i].y < min_y) {
                min_y = Intersections[i].y;
            }
        }
        
        center = new Point((max_x+min_x)/2, (max_y+min_y)/2);
        
//        double x = 0;
//        double y = 0;
//        for (int i = 0; i < Intersections.length; i++) {
//            x += Intersections[i].getIntersection().x;
//            y += Intersections[i].getIntersection().y;
//        }
//        center = new Point(x/Intersections.length, y/Intersections.length);
    }
    
    public Point getCenter() {
        return center;
    }
    
    public StraightLine[] getSLs() {
        return SLs;
    }
    
    public Point[] getIntersections() {
        return Intersections;
    }
    
    public Point getP(int i) {
        return Intersections[i];
    }
    
    public Polygon[] getPolygons() {
        return Polygons;
    }

    public Segment[] getSegments() {
        return Segments;
    }
    
}