/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colorationminimale;

import javax.swing.JFrame;

/**
 * @author guillaume.alabre et remi.fleurance
 */
public class ColorationMinimale {

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        StraightLine[] SLs = { // Les droites de la Scène
            new StraightLine(new Point(12,3), new Point(15,5)),
            new StraightLine(new Point(7,7), new Point(8,8)),
            new StraightLine(new Point(-2,9), new Point(15,5)),
            new StraightLine(new Point(17,13), new Point(13,22)),
            new StraightLine(new Point(4,8), new Point(9,3)),
            new StraightLine(new Point(-16,-5), new Point(-20,-15))
        };
        
        JFrame window= new JFrame( "Computational Geometry");
        window.setSize( 800 , 600 ) ;
        
        GeoFrame frame= new GeoFrame();
        
        Scene scene = new Scene(SLs, frame);
        frame.setScale(25);
        frame.setScene(scene);
        
        window.add( frame );
        
        window.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        window.setVisible( true );
        scene.run();
    }
    
}
